<?php
namespace App\site;

require_once ("../../vendor/autoload.php");
use App\dashbord\db\DB;
class Site extends DB{
    private $user_id;
    private $username;

    public function setData($data=''){
        if(array_key_exists('user_id',$data)){
            $this->user_id = $data['user_id'];
        }
        if(array_key_exists('username',$data)){
            $this->username = $data['username'];
        }
        return $this;
    }
    public function users(){
        try {
            $stmt = $this->pdo->prepare('SELECT * FROM `users` WHERE `username`=:username');
            $stmt->execute(array(':username' => $this->username));
            if($stmt){
                return $stmt->fetch();
            }
        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }
    public function settings($data){
        try {
            $sql = "SELECT * FROM `settings` WHERE `user_id`='$data'";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();
            if($stmt){
                return $data =  $stmt->fetch();
            }
        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }
    public function about($data){
        try {
            $sql = "SELECT * FROM `abouts` WHERE `user_id`='$data'";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();
            if($stmt){
                return $data =  $stmt->fetchAll();
            }
        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }
    public function education($data){
        try {
            $stmt = $this->pdo->prepare("SELECT * FROM `educations` WHERE `user_id`='$data'");
            $stmt->execute();
            if($stmt){
                return $stmt->fetchAll();
            }
        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }
    public function experiences($data){
        try {
            $stmt = $this->pdo->prepare("SELECT * FROM `experiences` WHERE `user_id`='$data'");
            $stmt->execute();
            if($stmt){
                return $stmt->fetchAll();
            }
        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }
    public function posts($data){
        try {
            $stmt = $this->pdo->prepare("SELECT * FROM `posts` WHERE `user_id`='$data'");
            $stmt->execute();
            if($stmt){
                return $stmt->fetchAll();
            }
        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }
}