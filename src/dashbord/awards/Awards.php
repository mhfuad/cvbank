<?php
namespace App\dashbord\awards;
require_once ("../../../vendor/autoload.php");
use App\dashbord\db\DB;

class Awards extends DB{
    private $user_id;
    private $title;
    private $organization;
    private $description;
    private $location;
    private $year;
    private $id;

    public function setData($data=''){
        if(array_key_exists('user_id', $data)){
            $this->user_id = $data['user_id'];
        }
        if(array_key_exists('title', $data)){
            $this->title = $data['title'];
        }
        if(array_key_exists('organization', $data)){
            $this->organization = $data['organization'];
        }
        if(array_key_exists('description', $data)){
            $this->description = $data['description'];
        }
        if(array_key_exists('location', $data)){
            $this->location = $data['location'];
        }
        if(array_key_exists('year', $data)){
            $this->year = $data['year'];
        }
        if(array_key_exists('id', $data)){
            $this->id = $data['id'];
        }
        return $this;
    }
    public function create(){
        try {
            $stmt = $this->pdo->prepare('INSERT INTO `awards`(`user_id`,
                                                              `title`,
                                                              `organization`,
                                                              `description`,
                                                              `location`,
                                                              `year`,
                                                              `created_at`)
                                    VALUES(:user_id,
                                          :title,
                                          :organization,
                                          :description,
                                          :location,
                                          :year,
                                          :created_at)');
            $stmt->bindParam(':user_id', $user_id);
            $stmt->bindParam(':title', $title);
            $stmt->bindParam(':organization', $organization);
            $stmt->bindParam(':description', $description);
            $stmt->bindParam(':location', $location);
            $stmt->bindParam(':year', $year);
            $stmt->bindParam(':created_at', $created_at);

            $user_id = $this->user_id;
            $title = $this->title;
            $organization = $this->organization;
            $description = $this->description;
            $location = $this->location;
            $year = $this->year;
            $created_at = date('Y-m-d h:m:s');

            $stmt->execute();
            if ($stmt){
                //$_SESSION['msg'] = "We have send you a varification mail. chack you mail for varification.";
                header('location: index.php');
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function show(){
        try {
            $stmt = $this->pdo->prepare("SELECT * FROM `awards` WHERE `user_id`=:user_id AND `deleted_at`='0000-00-00 00:00:00'");
            $stmt->bindParam(':user_id', $user_id);

            $user_id = $this->user_id;

            $stmt->execute();
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function edit($data){
        try {
            $stmt = $this->pdo->prepare("SELECT * FROM `awards` WHERE `id`='$data'");

            $stmt->execute();
            return $stmt->fetch();
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function update()
    {
        try {
            $stmt = $this->pdo->prepare("UPDATE `awards` SET `title`=:title,
                                                            `organization`=:organization,
                                                            `description`=:description,
                                                            `location`=:location, 
                                                            `year`=:year, 
                                                            `updated_at`=:updated_at 
                                                            WHERE `id`=:id");
            $stmt->bindParam(':title', $title);
            $stmt->bindParam(':organization', $organization);
            $stmt->bindParam(':description', $description);
            $stmt->bindParam(':location', $location);
            $stmt->bindParam(':year', $year);
            $stmt->bindParam(':updated_at', $updated_at);
            $stmt->bindParam(':id', $id);

            $title = $this->title;
            $organization = $this->organization;
            $description = $this->description;
            $location = $this->location;
            $year = $this->year;
            $updated_at = date('Y-m-d h:m:s');
            $id = $this->id;

            $stmt->execute();

            if ($stmt) {
                header('location: index.php');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function delete()
    {
        try {
            $stmt = $this->pdo->prepare("DELETE FROM `awards` WHERE `id`=:id");

            $stmt->bindParam(':id', $id);

            $id = $this->id;

            $stmt->execute();

            if ($stmt) {
                header('location: index.php');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}
