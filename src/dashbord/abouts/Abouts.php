<?php

namespace App\dashbord\abouts;

require_once ("../../../vendor/autoload.php");
use App\dashbord\db\DB;

class Abouts extends DB{
    private $user_id;
    private $title;
    private $phone;
    private $bio;
    private $id;

    public function setData($data=''){
        if(array_key_exists('user_id', $data)){
            $this->user_id = $data['user_id'];
        }
        if(array_key_exists('title', $data)){
            $this->title = $data['title'];
        }
        if(array_key_exists('phone', $data)){
            $this->phone = $data['phone'];
        }
        if(array_key_exists('bio', $data)){
            $this->bio = $data['bio'];
        }
        if(array_key_exists('id', $data)){
            $this->id = $data['id'];
        }
        return $this;
    }
    public function create(){
        try {
            $stmt = $this->pdo->prepare('INSERT INTO `abouts`(`user_id`,`title`,`phone`,`bio`,`created_at`)
                                    VALUES(:user_id,:title,:phone,:bio,:created_at)');
            $stmt->bindParam(':user_id', $user_id);
            $stmt->bindParam(':title', $title);
            $stmt->bindParam(':phone', $phone);
            $stmt->bindParam(':bio', $bio);
            $stmt->bindParam(':created_at', $created_at);

            $user_id = $this->user_id;
            $title = $this->title;
            $phone = $this->phone;
            $bio = $this->bio;
            $created_at = date('Y-m-d h:m:s');

            $stmt->execute();
            if ($stmt){
                header('location: index.php');
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function show() {
        try {
            $stmt = $this->pdo->prepare("SELECT * FROM `abouts` WHERE `user_id`=:user_id AND `deleted_at`='0000-00-00 00:00:00'");
            $stmt->bindParam(':user_id', $user_id);

            $user_id = $this->user_id;

            $stmt->execute();
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function edit(){
        try {
            //$sql = "";
            $stmt = $this->pdo->prepare("SELECT * FROM `abouts` WHERE `user_id`=':user_id'");
            $stmt->bindParam(':user_id', $user_id);
            $user_id = $this->user_id;
            $stmt->execute();
            return $stmt->fetch();

        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function update()
    {
        try {
            $stmt = $this->pdo->prepare('UPDATE `abouts` SET `title`=:title,`phone`=:phone,`bio`=:bio, `updated_at`=:updated_at WHERE `id`=:id');
            $stmt->bindParam(':title', $title);
            $stmt->bindParam(':phone', $phone);
            $stmt->bindParam(':bio', $bio);
            $stmt->bindParam(':updated_at', $updated_at);
            $stmt->bindParam(':id', $id);

            $title = $this->title;
            $phone = $this->phone;
            $bio = $this->bio;
            $updated_at = date('Y-m-d h:m:s');
            $id = $this->id;

            $stmt->execute();
            if ($stmt) {
                header('location: edit.php?unique_id='.$this->user_id);
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function delete(){
        try {
            $stmt = $this->pdo->prepare("UPDATE `abouts` SET `deleted_at`=:deleted_at WHERE `id`=:id");

            $stmt->bindParam(':id', $id);
            $stmt->bindParam(':deleted_at', $deleted_at);

            $id = $this->id;
            $deleted_at = date('Y-m-d h:m:s');

            $stmt->execute();

            if ($stmt) {
                header('location: index.php');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

}