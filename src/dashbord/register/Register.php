<?php
namespace App\dashbord\register;

require_once ("../../../vendor/autoload.php");
use App\dashbord\db\DB;
class Register extends DB{
    private $title;
    private $firstName;
    private $lastName;
    private $email;
    private $password;

    public function setData($data=''){
        if(array_key_exists('title', $data)){
            $this->title = $data['title'];
        }
        if(array_key_exists('firstName', $data)){
            $this->firstName = $data['firstName'];
        }
        if(array_key_exists('lastName', $data)){
            $this->lastName = $data['lastName'];
        }
        if(array_key_exists('password', $data)){
            $this->password = $data['password'];
        }
        if(array_key_exists('email', $data)){
            $this->email = $data['email'];
        }
        return $this;
    }
    public function register(){
        $token = sha1($this->title);
        try {
            $stmt = $this->pdo->prepare('INSERT INTO `users`(`unique_id`,`first_name`,`last_name`,`username`,`email`,`password`,`token`,`is_active`,`user_role`,`created_at`)
                                    VALUES(:uniq,:first,:last,:usr,:mail,:pass,:token,:activ,:role,:creat)');
            $stmt->bindParam(':uniq', $uniq);
            $stmt->bindParam(':first', $first);
            $stmt->bindParam(':last', $last);
            $stmt->bindParam(':usr', $usr);
            $stmt->bindParam(':mail', $mail);
            $stmt->bindParam(':pass', $pass);
            $stmt->bindParam(':token', $tok);
            $stmt->bindParam(':activ', $activ);
            $stmt->bindParam(':role', $role);
            $stmt->bindParam(':creat', $creat);

            $uniq = uniqid();
            $first = $this->firstName;
            $last = $this->lastName;
            $usr = $this->title;
            $mail = $this->email;
            $pass = $this->password;
            $tok = $token;
            $activ = 0;
            $role = 1;
            $creat = date('Y-m-d h:m:s');

            $stmt->execute();
            if ($stmt){
                $message = '<a href="localhost:82/cvbank/view/dashbord/register/verify.php?token=.$token">Click here to verify.</a>';
                mail($this->email, "Account Varification.", $message);
                $_SESSION['msg'] = "We have send you a varification mail. chack you mail for varification.";
                header('location: ../login/create.php');
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function getToken($token){
        try{
            $stmt = $this->pdo->prepare("SELECT * FROM `users` WHERE `token`='$token'");
            $stmt->execute();
            if ($stmt){
                return $data = $stmt->fetchAll();
            }
        }catch (PDOException $e){
            echo $e->getMessage();
        }
    }
    public function verify($id){
        try{
            $stmt = $this->pdo->prepare("UPDATE `users` SET `is_active`=1 WHERE `id`=$id");
            $stmt->execute();
            if ($stmt){
                $_SESSION['msg'] = "Verify success. You can login now";
                header('location: ../login/create.php');
            }
        }catch (PDOException $e){
            echo $e->getMessage();
        }
    }
}