<?php
namespace App\dashbord\skills;

require_once ("../../../vendor/autoload.php");
use App\dashbord\db\DB;

class Skills extends DB{
    private $user_id;
    private $title;
    private $description;
    private $level;
    private $experience;
    private $experience_area;
    private $id;

    public function setData($data=''){
        if(array_key_exists('user_id', $data)){
            $this->user_id = $data['user_id'];
        }
        if(array_key_exists('title', $data)){
            $this->title = $data['title'];
        }
        if(array_key_exists('description', $data)){
            $this->description = $data['description'];
        }
        if(array_key_exists('level', $data)){
            $this->level = $data['level'];
        }
        if(array_key_exists('experience', $data)){
            $this->experience = $data['experience'];
        }
        if(array_key_exists('experience_area', $data)){
            $this->experience_area = $data['experience_area'];
        }
        if(array_key_exists('id', $data)){
            $this->id = $data['id'];
        }
        return $this;
    }
    public function create(){
        try {
            $stmt = $this->pdo->prepare('INSERT INTO `skills`(`user_id`,`title`,`description`,`level`,`experience`,`experience_area`,`created_at`)
                                    VALUES(:user_id,:title,:description,:level,:experience,:experience_area,:created_at)');
            $stmt->bindParam(':user_id', $user_id);
            $stmt->bindParam(':title', $title);
            $stmt->bindParam(':description', $description);
            $stmt->bindParam(':level', $level);
            $stmt->bindParam(':experience', $experience);
            $stmt->bindParam(':experience_area', $experience_area);
            $stmt->bindParam(':created_at', $created_at);

            $user_id = $this->user_id;
            $title = $this->title;
            $description = $this->description;
            $level = $this->level;
            $experience = $this->experience;
            $experience_area = $this->experience_area;
            $created_at = date('Y-m-d h:m:s');

            $stmt->execute();
            if ($stmt){
                $_SESSION['msg'] = "We have send you a varification mail. chack you mail for varification.";
                header('location: index.php');
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function show(){
        try {
            $stmt = $this->pdo->prepare("SELECT * FROM `skills` WHERE `user_id`=:user_id AND `deleted_at`='0000-00-00 00:00:00'");
            $stmt->bindParam(':user_id', $user_id);

            $user_id = $this->user_id;

            $stmt->execute();
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function edit($data){
        try {
            $stmt = $this->pdo->prepare("SELECT * FROM `skills` WHERE `id`='$data'");

            $stmt->execute();
            return $stmt->fetch();
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function update()
    {
        try {
            $stmt = $this->pdo->prepare("UPDATE `skills` SET `title`=:title,
                                                            `description`=:description,
                                                            `level`=:level,
                                                            `experience`=:experience,
                                                            `experience_area`=:experience_area, 
                                                            `updated_at`=:updated_at 
                                                            WHERE `id`=:id");
            $stmt->bindParam(':title', $title);
            $stmt->bindParam(':description', $description);
            $stmt->bindParam(':level', $level);
            $stmt->bindParam(':experience', $experience);
            $stmt->bindParam(':experience_area', $experience_area);
            $stmt->bindParam(':updated_at', $updated_at);
            $stmt->bindParam(':id', $id);

            $title = $this->title;
            $description = $this->description;
            $level = $this->level;
            $experience = $this->experience;
            $experience_area = $this->experience_area;
            $updated_at = date('Y-m-d h:m:s');
            $id = $this->id;

            $stmt->execute();

            /*if ($stmt){
                $message = '<a href="localhost:82/cvbank/view/dashbord/register/verify.php?token=.$token">Click here to verify.</a>';
                mail($this->email, "Account Varification.", $message);
                $_SESSION['msg'] = "We have send you a varification mail. chack you mail for varification.";
                header('location: ../login/create.php');
            }*/
            if ($stmt) {
                header('location: index.php');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function delete()
    {
        try {
            $stmt = $this->pdo->prepare("DELETE FROM `skills` WHERE `id`=:id");

            $stmt->bindParam(':id', $id);

            $id = $this->id;

            $stmt->execute();

            if ($stmt) {
                header('location: index.php');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

}