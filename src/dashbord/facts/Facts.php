<?php

namespace App\dashbord\facts;

require_once ("../../../vendor/autoload.php");
use App\dashbord\db\DB;

class Facts extends DB{
    private $user_id;
    private $title;
    private $no_of_items;
    private $img;
    private $id;

    public function setData($data=''){
        if(array_key_exists('user_id', $data)){
            $this->user_id = $data['user_id'];
        }
        if(array_key_exists('title', $data)){
            $this->title = $data['title'];
        }
        if(array_key_exists('no_of_items', $data)){
            $this->no_of_items = $data['no_of_items'];
        }
        if(array_key_exists('img', $data)){
            $this->img = $data['img'];
        }
        if(array_key_exists('id', $data)){
            $this->id = $data['id'];
        }
        return $this;
    }
    public function create(){
        try {
            $stmt = $this->pdo->prepare('INSERT INTO `facts`(`user_id`,`title`,`no_of_items`,`img`,`created_at`)
                                    VALUES(:user_id,:title,:no_of_items,:img,:created_at)');
            $stmt->bindParam(':user_id', $user_id);
            $stmt->bindParam(':title', $title);
            $stmt->bindParam(':no_of_items', $no_of_items);
            $stmt->bindParam(':img', $img);
            $stmt->bindParam(':created_at', $created_at);

            $user_id = $this->user_id;
            $title = $this->title;
            $no_of_items = $this->no_of_items;
            $img = $this->img;
            $created_at = date('Y-m-d h:m:s');

            $stmt->execute();
            if ($stmt){
                $_SESSION['msg'] = "We have send you a varification mail. chack you mail for varification.";
                header('location: index.php');
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function show(){
        try {
            $stmt = $this->pdo->prepare('SELECT * FROM `facts` WHERE `user_id`=:user_id');
            $stmt->bindParam(':user_id', $user_id);

            $user_id = $this->user_id;

            $stmt->execute();
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function edit($data){
        try {
            $stmt = $this->pdo->prepare("SELECT * FROM `facts` WHERE `id`='$data'");

            $stmt->execute();
            return $stmt->fetch();
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function update()
    {
        try {
            $stmt = $this->pdo->prepare('UPDATE `facts` SET `title`=:title,`no_of_items`=:no_of_items,`img`=:img, `updated_at`=:updated_at WHERE `id`=:id');
            $stmt->bindParam(':title', $title);
            $stmt->bindParam(':no_of_items', $no_of_items);
            $stmt->bindParam(':img', $img);
            $stmt->bindParam(':updated_at', $updated_at);
            $stmt->bindParam(':id', $id);

            $title = $this->title;
            $no_of_items = $this->no_of_items;
            $img = $this->img;
            $updated_at = date('Y-m-d h:m:s');
            $id = $this->id;

            $stmt->execute();
            /*if ($stmt){
                $message = '<a href="localhost:82/cvbank/view/dashbord/register/verify.php?token=.$token">Click here to verify.</a>';
                mail($this->email, "Account Varification.", $message);
                $_SESSION['msg'] = "We have send you a varification mail. chack you mail for varification.";
                header('location: ../login/create.php');
            }*/
            if ($stmt) {
                header('location: index.php');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function delete(){
        try {
            $stmt = $this->pdo->prepare("DELETE FROM `facts` WHERE `id`=:id");

            $stmt->bindParam(':id', $id);

            $id = $this->id;

            $stmt->execute();

            if ($stmt) {
                header('location: index.php');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}