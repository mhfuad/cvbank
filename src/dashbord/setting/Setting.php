<?php
namespace App\dashbord\setting;

require_once ("../../../vendor/autoload.php");
use App\dashbord\db\DB;
class Setting extends DB{
    private $user_id;
    private $title;
    private $fullName;
    private $description;
    private $address;
    private $fileName;
    private $delete;

    public function setData($data=''){
        if(array_key_exists('user_id', $data)){
            $this->user_id = $data['user_id'];
        }
        if(array_key_exists('title', $data)){
            $this->title = $data['title'];
        }
        if(array_key_exists('fullName', $data)){
            $this->fullName = $data['fullName'];
        }
        if(array_key_exists('description', $data)){
            $this->description = $data['description'];
        }
        if(array_key_exists('address', $data)){
            $this->address = $data['address'];
        }
        if(array_key_exists('fileName', $data)){
            $this->fileName = $data['fileName'];
        }
        if(array_key_exists('unique_id', $data)){
            $this->delete = $data['unique_id'];
        }
        return $this;
    }
    public function creat(){
        try {
            $stmt = $this->pdo->prepare('INSERT INTO `settings`(`user_id`,`title`,`fullname`,`description`,`address`,`created_at`)
                                    VALUES(:user_id,:title,:fullname,:description,:address,:created_at)');
            $stmt->bindParam(':user_id', $user_id);
            $stmt->bindParam(':title', $title);
            $stmt->bindParam(':fullname', $fullname);
            $stmt->bindParam(':description', $description);
            $stmt->bindParam(':address', $address);
            $stmt->bindParam(':created_at', $created_at);

            $user_id = $this->user_id;
            $title = $this->title;
            $fullname = $this->fullName;
            $description = $this->description;
            $address = $this->address;
            $created_at = date('Y-m-d h:m:s');

            $stmt->execute();
            if ($stmt){
                header('location: ../setting/index.php');
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function fileUpdate(){
        try {
            $stmt = $this->pdo->prepare('UPDATE `settings` SET `featured_img`=:featured_img WHERE `user_id`=:user_id');
            $stmt->bindParam(':featured_img', $featured_img);
            $stmt->bindParam(':user_id', $user_id);

            $featured_img = $this->fileName;
            $user_id = $this->user_id;

            $stmt->execute();
            /*if ($stmt){
                $message = '<a href="localhost:82/cvbank/view/dashbord/register/verify.php?token=.$token">Click here to verify.</a>';
                mail($this->email, "Account Varification.", $message);
                $_SESSION['msg'] = "We have send you a varification mail. chack you mail for varification.";
                header('location: ../login/create.php');
            }*/
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function show(){
        try {
            $stmt = $this->pdo->prepare("SELECT * FROM `settings` WHERE `user_id`=:user_id AND `deleted_at`='0000-00-00 00:00:00'");
            $stmt->bindParam(':user_id', $user_id);

            $user_id = $this->user_id;

            $stmt->execute();
            return $stmt->fetch();
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function edit($data){
        try {
            $stmt = $this->pdo->prepare("SELECT * FROM `settings` WHERE `user_id`='$data' AND `deleted_at`='0000-00-00 00:00:00'");

            $stmt->execute();
            return $stmt->fetch();
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function update(){
        try {
            $stmt = $this->pdo->prepare('UPDATE `settings` SET `title`=:title,`fullname`=:fullname,`description`=:description,`address`=:address,`updated_at`=:updated_at WHERE `user_id`=:user_id');
            $stmt->bindParam(':title', $title);
            $stmt->bindParam(':fullname', $fullname);
            $stmt->bindParam(':description', $description);
            $stmt->bindParam(':address', $address);
            $stmt->bindParam(':updated_at', $updated_at);
            $stmt->bindParam(':user_id', $user_id);

            $title = $this->title;
            $fullname = $this->fullName;
            $description = $this->description;
            $address = $this->address;
            $updated_at = date('Y-m-d h:m:s');
            $user_id = $this->user_id;

            $stmt->execute();
            /*if ($stmt){
                $message = '<a href="localhost:82/cvbank/view/dashbord/register/verify.php?token=.$token">Click here to verify.</a>';
                mail($this->email, "Account Varification.", $message);
                $_SESSION['msg'] = "We have send you a varification mail. chack you mail for varification.";
                header('location: ../login/create.php');
            }*/
            if ($stmt) {
                header('location: edit.php?unique_id='.$user_id);
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function delete(){
        try {
            $stmt = $this->pdo->prepare('UPDATE `settings` SET `deleted_at`=:deleted_at WHERE `user_id`=:user_id');
            $stmt->bindParam(':deleted_at', $deleted_at);
            $stmt->bindParam(':user_id', $user_id);

            $deleted_at = date('Y-m-d h:m:s');
            $user_id = $this->delete;

            $stmt->execute();
            /*if ($stmt){
                $message = '<a href="localhost:82/cvbank/view/dashbord/register/verify.php?token=.$token">Click here to verify.</a>';
                mail($this->email, "Account Varification.", $message);
                $_SESSION['msg'] = "We have send you a varification mail. chack you mail for varification.";
                header('location: ../login/create.php');
            }*/
            if ($stmt) {
                header('location: index.php');
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

}