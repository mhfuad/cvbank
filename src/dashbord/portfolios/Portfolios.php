<?php

namespace App\dashbord\portfolios;
require_once ("../../../vendor/autoload.php");
use App\dashbord\db\DB;
class Portfolios extends DB{
    private $user_id;
    private $title;
    private $description;
    private $img;
    private $categories;
    private $id;

    public function setData($data=''){
        if(array_key_exists('user_id', $data)){
            $this->user_id = $data['user_id'];
        }
        if(array_key_exists('title', $data)){
            $this->title = $data['title'];
        }
        if(array_key_exists('description', $data)){
            $this->description = $data['description'];
        }
        if(array_key_exists('img', $data)){
            $this->img = $data['img'];
        }
        if(array_key_exists('categories', $data)){
            $this->categories = $data['categories'];
        }
        if(array_key_exists('id', $data)){
            $this->id = $data['id'];
        }
        return $this;
    }
    public function create(){
        try {
            $stmt = $this->pdo->prepare('INSERT INTO `portfolios`(`user_id`,
                                                                `title`,
                                                                `description`,
                                                                `img`,
                                                                `categories`,
                                                                `created_at`)
                                    VALUES(:user_id,
                                    :title,
                                    :description,
                                    :img,
                                    :categories,
                                    :created_at)');
            $stmt->bindParam(':user_id', $user_id);
            $stmt->bindParam(':title', $title);
            $stmt->bindParam(':description', $description);
            $stmt->bindParam(':img', $img);
            $stmt->bindParam(':categories', $categories);
            $stmt->bindParam(':created_at', $created_at);

            $user_id = $this->user_id;
            $title = $this->title;
            $description = $this->description;
            $img = $this->img;
            $categories = $this->categories;
            $created_at = date('Y-m-d h:m:s');

            $stmt->execute();
            if ($stmt){
                //$_SESSION['msg'] = "We have send you a varification mail. chack you mail for varification.";
                header('location: index.php');
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function show(){
        try {
            $stmt = $this->pdo->prepare("SELECT * FROM `portfolios` WHERE `user_id`=:user_id AND `deleted_at`='0000-00-00 00:00:00'");
            $stmt->bindParam(':user_id', $user_id);

            $user_id = $this->user_id;

            $stmt->execute();
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function edit($data){
        try {
            $stmt = $this->pdo->prepare("SELECT * FROM `portfolios` WHERE `id`='$data'");

            $stmt->execute();
            return $stmt->fetch();
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function update()
    {
        try {
            $stmt = $this->pdo->prepare("UPDATE `portfolios` SET `title`=:title,
                                                            `description`=:description,
                                                            `img`=:img, 
                                                            `categories`=:categories, 
                                                            `updated_at`=:updated_at 
                                                            WHERE `id`=:id");
            $stmt->bindParam(':title', $title);
            $stmt->bindParam(':description', $description);
            $stmt->bindParam(':img', $img);
            $stmt->bindParam(':categories', $categories);
            $stmt->bindParam(':updated_at', $updated_at);
            $stmt->bindParam(':id', $id);

            $title = $this->title;
            $description = $this->description;
            $img = $this->img;
            $categories = $this->categories;
            $updated_at = date('Y-m-d h:m:s');
            $id = $this->id;

            $stmt->execute();

            if ($stmt) {
                header('location: index.php');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function delete(){
        try {
            $stmt = $this->pdo->prepare("DELETE FROM `portfolios` WHERE `id`=:id");

            $stmt->bindParam(':id', $id);

            $id = $this->id;

            $stmt->execute();

            if ($stmt) {
                header('location: index.php');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}