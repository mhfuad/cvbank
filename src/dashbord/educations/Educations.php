<?php
namespace App\dashbord\Educations;
require_once ("../../../vendor/autoload.php");
use App\dashbord\db\DB;

class Educations extends DB{
    private $user_id;
    private $title;
    private $institute;
    private $result;
    private $passing_year;
    private $main_subject;
    private $education_board;
    private $course_duration;
    private $id;

    public function setData($data=''){
        if(array_key_exists('user_id', $data)){
            $this->user_id = $data['user_id'];
        }
        if(array_key_exists('title', $data)){
            $this->title = $data['title'];
        }
        if(array_key_exists('institute', $data)){
            $this->institute = $data['institute'];
        }
        if(array_key_exists('result', $data)){
            $this->result = $data['result'];
        }
        if(array_key_exists('passing_year', $data)){
            $this->passing_year = $data['passing_year'];
        }
        if(array_key_exists('main_subject', $data)){
            $this->main_subject = $data['main_subject'];
        }
        if(array_key_exists('education_board', $data)){
            $this->education_board = $data['education_board'];
        }
        if(array_key_exists('course_duration', $data)){
            $this->course_duration = $data['course_duration'];
        }
        if(array_key_exists('id', $data)){
            $this->id = $data['id'];
        }
        return $this;
    }
    public function create(){
        try {
            $stmt = $this->pdo->prepare('INSERT INTO `educations`(`user_id`,
                                                              `title`,
                                                              `institute`,
                                                              `result`,
                                                              `passing_year`,
                                                              `main_subject`,
                                                              `education_board`,
                                                              `course_duration`,
                                                              `created_at`)
                                    VALUES(:user_id,
                                          :title,
                                          :institute,
                                          :result,
                                          :passing_year,
                                          :main_subject,
                                          :education_board,
                                          :course_duration,
                                          :created_at)');
            $stmt->bindParam(':user_id', $user_id);
            $stmt->bindParam(':title', $title);
            $stmt->bindParam(':institute', $institute);
            $stmt->bindParam(':result', $result);
            $stmt->bindParam(':passing_year', $passing_year);
            $stmt->bindParam(':main_subject', $main_subject);
            $stmt->bindParam(':education_board', $education_board);
            $stmt->bindParam(':course_duration', $course_duration);
            $stmt->bindParam(':created_at', $created_at);

            $user_id = $this->user_id;
            $title = $this->title;
            $institute = $this->institute;
            $result = $this->result;
            $passing_year = $this->passing_year;
            $main_subject = $this->main_subject;
            $education_board = $this->education_board;
            $course_duration = $this->course_duration;
            $created_at = date('Y-m-d h:m:s');

            $stmt->execute();
            if ($stmt){
                //$_SESSION['msg'] = "We have send you a varification mail. chack you mail for varification.";
                header('location: index.php');
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function show(){
        try {
            $stmt = $this->pdo->prepare("SELECT * FROM `educations` WHERE `user_id`=:user_id AND `deleted_at`='0000-00-00 00:00:00'");
            $stmt->bindParam(':user_id', $user_id);

            $user_id = $this->user_id;

            $stmt->execute();
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function edit($data){
        try {
            $stmt = $this->pdo->prepare("SELECT * FROM `educations` WHERE `id`='$data'");
            $stmt->execute();
            return $stmt->fetch();
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function update(){
        try {
            $stmt = $this->pdo->prepare("UPDATE `educations` SET `title`=:title,
                                                            `institute`=:institute,
                                                            `result`=:result,
                                                            `passing_year`=:passing_year, 
                                                            `main_subject`=:main_subject, 
                                                            `education_board`=:education_board, 
                                                            `course_duration`=:course_duration, 
                                                            `updated_at`=:updated_at 
                                                            WHERE `id`=:id");
            $stmt->bindParam(':title', $title);
            $stmt->bindParam(':institute', $institute);
            $stmt->bindParam(':result', $result);
            $stmt->bindParam(':passing_year', $passing_year);
            $stmt->bindParam(':main_subject', $main_subject);
            $stmt->bindParam(':education_board', $education_board);
            $stmt->bindParam(':course_duration', $course_duration);
            $stmt->bindParam(':updated_at', $updated_at);
            $stmt->bindParam(':id', $id);

            $title = $this->title;
            $institute = $this->institute;
            $result = $this->result;
            $passing_year = $this->passing_year;
            $main_subject = $this->main_subject;
            $main_subject = $this->education_board;
            $course_duration = $this->course_duration;
            $updated_at = date('Y-m-d h:m:s');
            $id = $this->id;

            $stmt->execute();

            if ($stmt) {
                header('location: index.php');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function delete()
    {
        try {
            $stmt = $this->pdo->prepare("DELETE FROM `educations` WHERE `id`=:id");

            $stmt->bindParam(':id', $id);

            $id = $this->id;

            $stmt->execute();

            if ($stmt) {
                header('location: index.php');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}