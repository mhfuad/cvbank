<?php
namespace App\dashbord\db;
use PDO;
class Db{
    protected $pdo;
    public function __construct(){
        try{
            $this->pdo = new PDO('mysql:host=localhost;dbname=cvbank','root','');
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }catch (\PDOException $e){
            echo "Error :". $e->getMessage();
        }
        session_start();
        return $this->pdo;
    }
}