<?php
namespace App\dashbord\experiences;

require_once ("../../../vendor/autoload.php");
use App\dashbord\db\DB;

class Experiences extends DB{
    private $user_id;
    private $designation;
    private $company_name;
    private $start_date;
    private $end_date;
    private $company_location;
    private $id;

    public function setData($data=''){
        if(array_key_exists('user_id', $data)){
            $this->user_id = $data['user_id'];
        }
        if(array_key_exists('designation', $data)){
            $this->designation = $data['designation'];
        }
        if(array_key_exists('company_name', $data)){
            $this->company_name = $data['company_name'];
        }
        if(array_key_exists('start_date', $data)){
            $this->start_date = $data['start_date'];
        }
        if(array_key_exists('end_date', $data)){
            $this->end_date = $data['end_date'];
        }
        if(array_key_exists('company_location', $data)){
            $this->company_location = $data['company_location'];
        }
        if(array_key_exists('id', $data)){
            $this->id = $data['id'];
        }
        return $this;
    }
    public function create(){
        try {
            $stmt = $this->pdo->prepare('INSERT INTO `experiences`(`user_id`,
                                                                `designation`,
                                                            `company_name`,
                                                            `start_date`,
                                                            `end_date`,
                                                            `company_location`,
                                                            `created_at`)
                                    VALUES(:user_id,
                                    :designation,
                                    :company_name,
                                    :start_date,
                                    :end_date,
                                    :company_location,
                                    :created_at)');
            $stmt->bindParam(':user_id', $user_id);
            $stmt->bindParam(':designation', $designation);
            $stmt->bindParam(':company_name', $company_name);
            $stmt->bindParam(':start_date', $start_date);
            $stmt->bindParam(':end_date', $end_date);
            $stmt->bindParam(':company_location', $company_location);
            $stmt->bindParam(':created_at', $created_at);

            $user_id = $this->user_id;
            $designation = $this->designation;
            $company_name = $this->company_name;
            $start_date = $this->start_date;
            $end_date = $this->end_date;
            $company_location = $this->company_location;
            $created_at = date('Y-m-d h:m:s');

            $stmt->execute();
            if ($stmt){
                $_SESSION['msg'] = "We have send you a varification mail. chack you mail for varification.";
                header('location: index.php');
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function show(){
        try {
            $stmt = $this->pdo->prepare("SELECT * FROM `experiences` WHERE `user_id`=:user_id AND `deleted_at`='0000-00-00 00:00:00'");
            $stmt->bindParam(':user_id', $user_id);

            $user_id = $this->user_id;

            $stmt->execute();
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function edit($data){
        try {
            $stmt = $this->pdo->prepare("SELECT * FROM `experiences` WHERE `id`='$data'");

            $stmt->execute();
            return $stmt->fetch();
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function update()
    {
        try {
            $stmt = $this->pdo->prepare("UPDATE `experiences` SET `designation`=:designation,
                                                            `company_name`=:company_name,
                                                            `start_date`=:start_date,
                                                            `end_date`=:end_date,
                                                            `company_location`=:company_location,
                                                            `updated_at`=:updated_at
                                                            WHERE `id`=:id");
            $stmt->bindParam(':designation', $designation);
            $stmt->bindParam(':company_name', $company_name);
            $stmt->bindParam(':start_date', $start_date);
            $stmt->bindParam(':end_date', $end_date);
            $stmt->bindParam(':company_location', $company_location);
            $stmt->bindParam(':updated_at', $updated_at);
            $stmt->bindParam(':id', $id);

            $designation = $this->designation;
            $company_name = $this->company_name;
            $start_date = $this->start_date;
            $end_date = $this->end_date;
            $company_location = $this->company_location;
            $updated_at = date('Y-m-d h:m:s');
            $id = $this->id;

            $stmt->execute();

            /*if ($stmt){
                $message = '<a href="localhost:82/cvbank/view/dashbord/register/verify.php?token=.$token">Click here to verify.</a>';
                mail($this->email, "Account Varification.", $message);
                $_SESSION['msg'] = "We have send you a varification mail. chack you mail for varification.";
                header('location: ../login/create.php');
            }*/
            if ($stmt) {
                header('location: index.php');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function delete()
    {
        try {
            $stmt = $this->pdo->prepare("DELETE FROM `experiences` WHERE `id`=:id");

            $stmt->bindParam(':id', $id);

            $id = $this->id;

            $stmt->execute();

            if ($stmt) {
                header('location: index.php');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

}