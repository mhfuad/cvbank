<?php
/*echo "<pre>";
print_r($_SESSION);
die();*/
?>
<div class="sidebar sidebar-main">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="category-content">
                <div class="media">
                    <a href="../setting/index.php" class="media-left"><img src="<?php if(!empty($_SESSION['img'])){echo "../profil_pic/".$_SESSION['img'];}else{echo "assets/images/image.png";} ?>" class="img-circle img-sm" alt=""></a>
                    <div class="media-body">
                        <span class="media-heading text-semibold"><?php echo $_SESSION['user_info']['username']; ?></span>
                        <div class="text-size-mini text-muted">
                            <i class="icon-pin text-size-small"></i> &nbsp;<?php echo $_SESSION['user_info']['address'] ?>
                        </div>
                    </div>

                    <div class="media-right media-middle">
                        <ul class="icons-list">
                            <li>
                                <a href="index.php"><i class="icon-cog3"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->
        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <!-- Main -->
                    <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                    <li class="active"><a href="../setting/edit.php?unique_id=<?php echo $_SESSION['user_info']['unique_id']; ?>"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                    <li >
                        <a href="../setting/edit.php?unique_id=<?php echo $_SESSION['user_info']['unique_id']; ?>"><i class="icon-gear"></i> <span>My setting</span></a>
                    </li>
                    <li>
                        <a href="../abouts/edit.php?unique_id=<?php echo $_SESSION['user_info']['unique_id']; ?>"><i class="icon-people"></i> <span>My about</span></a>
                        <!--<ul>
                            <li><a href="../abouts/index.php">Index</a></li>
                            <li><a href="../abouts/create.php">Create</a></li>
                        </ul>-->
                    </li>
                    <li>
                        <a href="../awards/index.php"><i class="glyphicon-briefcase"></i> <span>Awards</span></a>
                        <ul>
                            <li><a href="../awards/index.php">Index</a></li>
                            <li><a href="../awards/create.php">Create</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="../facts/index.php"><i class="glyphicon-briefcase"></i> <span>Facts</span></a>
                        <ul>
                            <li><a href="../facts/index.php">Index</a></li>
                            <li><a href="../facts/create.php">Create</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="../experiences/index.php"><i class="glyphicon-briefcase"></i> <span>Experiences</span></a>
                        <ul>
                            <li><a href="../experiences/index.php">Index</a></li>
                            <li><a href="../experiences/create.php">Create</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="../educations/index.php"><i class="glyphicon-education"></i> <span>Education</span></a>
                        <ul>
                            <li><a href="../educations/index.php">Index</a></li>
                            <li><a href="../educations/create.php">Create</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="../hobbies/index.php"><i class="icon-puzzle2"></i> <span>Hobbies</span></a>
                        <ul>
                            <li><a href="../hobbies/index.php">Index</a></li>
                            <li><a href="../hobbies/create.php">Create</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="../skills/index.php"><i class="icon-copy"></i> <span>Skills</span></a>
                        <ul>
                            <li><a href="../skills/index.php">Index</a></li>
                            <li><a href="../skills/create.php">Create</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="../services/index.php"><i class="icon-crop2"></i> <span>Services</span></a>
                        <ul>
                            <li><a href="../services/index.php">Index</a></li>
                            <li><a href="../services/create.php">Create</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="../posts/index.php"><i class="icon-crop2"></i> <span>Posts</span></a>
                        <ul>
                            <li><a href="../posts/index.php">Index</a></li>
                            <li><a href="../posts/create.php">Create</a></li>
                        </ul>
                    </li>
                    <!--<li>
                        <a href="#"><i class="icon-stack2"></i> <span>Page layouts</span></a>
                        <ul>
                            <li><a href="layout_navbar_fixed.html">Fixed navbar</a></li>
                            <li><a href="layout_navbar_sidebar_fixed.html">Fixed navbar &amp; sidebar</a></li>
                            <li><a href="layout_sidebar_fixed_native.html">Fixed sidebar native scroll</a></li>
                            <li><a href="layout_navbar_hideable.html">Hideable navbar</a></li>
                            <li><a href="layout_navbar_hideable_sidebar.html">Hideable &amp; fixed sidebar</a></li>
                            <li><a href="layout_footer_fixed.html">Fixed footer</a></li>
                            <li class="navigation-divider"></li>
                            <li><a href="boxed_default.html">Boxed with default sidebar</a></li>
                            <li><a href="boxed_mini.html">Boxed with mini sidebar</a></li>
                            <li><a href="boxed_full.html">Boxed full width</a></li>
                        </ul>
                    </li>-->
                    <!-- /page kits -->
                </ul>
            </div>
        </div>
        <!-- /main navigation -->
    </div>
</div>