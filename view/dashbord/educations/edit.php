<?php
require_once ("../../../vendor/autoload.php");
use App\dashbord\educations\Educations;

$obj = new Educations();
$arr = $obj->edit($_GET['id']);

$user_info = $_SESSION['user_info'];

/*echo "<pre>";
print_r($arr);
die();*/

include_once ("../content/head.php");
?>
<body>
<?php  include_once ("../content/main_navbar.php"); ?>


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <?php  include_once ("../content/sidebar.php"); ?>
        <div class="content-wrapper">
            <div class="content">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-lg table-hover table-striped">
                                <tbody>
                                <form action="update.php" method="post">
                                    <input type="hidden" name="id" class="form-control" value="<?php echo $arr['id']?>">
                                    <tr>
                                        <td class="col-md-1 col-sm-1">Institute</td>
                                        <td><input name="institute" class="form-control" type="text" value="<?php echo $arr['institute']?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-2 col-sm-2">Result</td>
                                        <td><input name="result" class="form-control" type="text" value="<?php echo $arr['result']?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-3 col-sm-3">Passing Year</td>
                                        <td><input name="passing_year" class="form-control" type="text" value="<?php echo $arr['passing_year']?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-2 col-sm-2">Main Subject</td>
                                        <td><input name="main_subject" class="form-control" type="text" value="<?php echo $arr['main_subject']?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-2 col-sm-2">Education Board</td>
                                        <td><input name="education_board" class="form-control" type="text" value="<?php echo $arr['education_board']?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-2 col-sm-2">Course Duration </td>
                                        <td><input name="course_duration" class="form-control" type="text" value="<?php echo $arr['course_duration']?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-2 col-sm-2">Action</td>
                                        <td><input type="submit" value="Update" class="btn btn-success"></td>
                                    </tr>
                                </form>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>

