<?php
require_once ("../../../vendor/autoload.php");
use App\dashbord\educations\Educations;

$obj = new Educations();
$user_info = $_SESSION['user_info'];

$data['user_id'] = $_SESSION['user_info']['unique_id'];

$arr = $obj->setData($data)->show();

include_once ("../content/head.php");
?>
<body>
<?php  include_once ("../content/main_navbar.php"); ?>

<!-- Page container -->
<div class="page-container">
    <!-- Page content -->
    <div class="page-content">
        <?php  include_once ("../content/sidebar.php"); ?>
        <div class="content-wrapper">
            <div class="content">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <h1 class="text-center">About </h1>
                        <div class="table-responsive">
                            <table class="table table-bordered table-lg table-hover table-striped">
                                <tbody>

                                <tr class="active">
                                    <td class="col-md-2 col-sm-2">Title</td>
                                    <td class="col-md-2 col-sm-2">Result</td>
                                    <td class="col-md-2 col-sm-2">Passing Year</td>
                                    <td class="col-md-2 col-sm-2">Main Subject</td>
                                    <td class="col-md-3 col-sm-3">Education Board</td>
                                    <td class="col-md-3 col-sm-3">Course Duration</td>
                                    <td class="col-md-2 col-sm-2">Action</td>
                                </tr>
                                <?php foreach ($arr as $a){?>
                                    <tr>
                                        <td><?php echo $a['title']?></td>
                                        <td><?php echo $a['result']?></td>
                                        <td><?php echo $a['passing_year']?></td>
                                        <td><?php echo $a['main_subject']?></td>
                                        <td><?php echo $a['education_board']?></td>
                                        <td><?php echo $a['course_duration']?></td>
                                        <td>
                                            <ul class="icons-list">
                                                <li><a href="edit.php?id=<?php echo $a['id'] ?>"><i class="icon-pencil7"></i></a></li>
                                                <!--<li><a href="#"><i class="icon-cog7"></i></a></li>-->
                                                <li><a href="delete.php?id=<?php echo $a['id'] ?>"><i class="icon-trash"></i></a></li>
                                            </ul>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
