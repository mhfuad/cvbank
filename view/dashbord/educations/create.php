<?php
require_once ("../../../vendor/autoload.php");
use App\dashbord\educations\Educations;

$obj = new Educations();


include_once ("../content/head.php");
?>
<body>
<?php  include_once ("../content/main_navbar.php"); ?>


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <?php  include_once ("../content/sidebar.php"); ?>
        <div class="content-wrapper">
            <div class="content">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <form class="form-horizontal" action="store.php" method="post">
                            <fieldset class="content-group">
                                <legend class="text-bold">About Page</legend>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Titel</label>
                                    <div class="col-lg-10">
                                        <input type="text" name="title" class="form-control" placeholder="Title">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Institute</label>
                                        <div class="col-lg-10">
                                            <input type="text" name="institute" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Result</label>
                                        <div class="col-lg-10">
                                            <input type="text" name="result" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Passing year</label>
                                        <div class="col-lg-10">
                                            <input type="text" name="passing_year" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Main Subject</label>
                                        <div class="col-lg-10">
                                            <input type="text" name="main_subject" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Education board</label>
                                        <div class="col-lg-10">
                                            <input type="text" name="education_board"  class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Course duration</label>
                                        <div class="col-lg-10">
                                            <input type="text" name="course_duration"  class="form-control">
                                        </div>
                                    </div>
                                </div>

                            </fieldset>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
