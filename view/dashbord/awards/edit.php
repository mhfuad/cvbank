<?php
require_once ("../../../vendor/autoload.php");
use App\dashbord\awards\Awards;

$obj = new Awards();
$arr = $obj->edit($_GET['id']);

$user_info = $_SESSION['user_info'];

/*echo "<pre>";
print_r($arr);
die();*/

include_once ("../content/head.php");
?>
<body>
<?php  include_once ("../content/main_navbar.php"); ?>


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <?php  include_once ("../content/sidebar.php"); ?>
        <div class="content-wrapper">
            <div class="content">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-lg table-hover table-striped">
                                <tbody>
                                <form action="update.php" method="post">
                                    <input type="hidden" name="id" class="form-control" value="<?php echo $arr['id']?>">
                                    <tr>
                                        <td class="col-md-1 col-sm-1">Title</td>
                                        <td><input name="title" class="form-control" type="text" value="<?php echo $arr['title']?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-2 col-sm-2">Organization</td>
                                        <td><input name="organization" class="form-control" type="text" value="<?php echo $arr['organization']?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-2 col-sm-2">Description</td>
                                        <td><input name="description" class="form-control" type="text" value="<?php echo $arr['description']?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-3 col-sm-3">Location</td>
                                        <td><input name="location" class="form-control" type="text" value="<?php echo $arr['location']?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-2 col-sm-2">Year</td>
                                        <td><input name="year" class="form-control" type="text" value="<?php echo $arr['year']?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-2 col-sm-2">Action</td>
                                        <td><input type="submit" value="Update" class="btn btn-success"></td>
                                    </tr>
                                </form>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>




<tr class="active">
    <td class="col-md-2 col-sm-2">Title</td>
    <td class="col-md-2 col-sm-2">Organization</td>
    <td class="col-md-3 col-sm-3">Description</td>
    <td class="col-md-2 col-sm-2">Location</td>
    <td class="col-md-3 col-sm-3">Year</td>
    <td class="col-md-2 col-sm-2">Action</td>
</tr>

