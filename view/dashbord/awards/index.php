<?php
require_once ("../../../vendor/autoload.php");
use App\dashbord\awards\Awards;

$obj = new Awards();
$user_info = $_SESSION['user_info'];

$data['user_id'] = $_SESSION['user_info']['unique_id'];

$arr = $obj->setData($data)->show();

include_once ("../content/head.php");
?>
<body>
<?php  include_once ("../content/main_navbar.php"); ?>


<!-- Page container -->
<div class="page-container">
    <!-- Page content -->
    <div class="page-content">
        <?php  include_once ("../content/sidebar.php"); ?>
        <div class="content-wrapper">
            <div class="content">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <h1 class="text-center">About </h1>
                        <div class="table-responsive">
                            <table class="table table-bordered table-lg table-hover table-striped">
                                <tbody>

                                <tr class="active">
                                    <td class="col-md-2 col-sm-2">Title</td>
                                    <td class="col-md-2 col-sm-2">Organization</td>
                                    <td class="col-md-2 col-sm-2">Description</td>
                                    <td class="col-md-2 col-sm-2">Location</td>
                                    <td class="col-md-3 col-sm-3">Year</td>
                                    <td class="col-md-2 col-sm-2">Action</td>
                                </tr>
                                <?php foreach ($arr as $a){?>
                                    <tr>
                                        <td><?php echo $a['title']?></td>
                                        <td><?php echo $a['organization']?></td>
                                        <td><?php echo $a['description']?></td>
                                        <td><?php echo $a['location']?></td>
                                        <td><?php echo $a['year']?></td>
                                        <td>
                                            <ul class="icons-list">
                                                <li><a href="edit.php?id=<?php echo $a['id'] ?>"><i class="icon-pencil7"></i></a></li>
                                                <!--<li><a href="#"><i class="icon-cog7"></i></a></li>-->
                                                <li><a href="delete.php?id=<?php echo $a['id'] ?>"><i class="icon-trash"></i></a></li>
                                            </ul>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
