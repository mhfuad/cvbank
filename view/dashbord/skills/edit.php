<?php
require_once ("../../../vendor/autoload.php");
use App\dashbord\skills\Skills;

$obj = new Skills();
$arr = $obj->edit($_GET['id']);

$user_info = $_SESSION['user_info'];

/*echo "<pre>";
print_r($arr);
die();*/

include_once ("../content/head.php");
?>
<body>
<?php  include_once ("../content/main_navbar.php"); ?>


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <?php  include_once ("../content/sidebar.php"); ?>
        <div class="content-wrapper">
            <div class="content">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-lg table-hover table-striped">
                                <tbody>
                                <form action="update.php" method="post">
                                    <input type="hidden" name="id" class="form-control" value="<?php echo $arr['id']?>">
                                <tr>
                                    <td class="col-md-1 col-sm-1">Title</td>
                                    <td><input name="title" class="form-control" type="text" value="<?php echo $arr['title']?>"></td>
                                </tr>
                                <tr>
                                    <td class="col-md-2 col-sm-2">Description</td>
                                    <td><input name="description" class="form-control" type="text" value="<?php echo $arr['description']?>"></td>
                                </tr>
                                <tr>
                                    <td class="col-md-3 col-sm-3">Level</td>
                                    <td><input name="level" class="form-control" type="text" value="<?php echo $arr['level']?>"></td>
                                </tr>
                                <tr>
                                    <td class="col-md-2 col-sm-2">Experience</td>
                                    <td><input name="experience" class="form-control" type="text" value="<?php echo $arr['experience']?>"></td>
                                </tr>
                                <tr>
                                    <td class="col-md-2 col-sm-2">Experience_area</td>
                                    <td><input name="experience_area" class="form-control" type="text" value="<?php echo $arr['experience_area']?>"></td>
                                </tr>
                                <tr>
                                    <td class="col-md-2 col-sm-2">Action</td>
                                    <td><input type="submit" value="Update" class="btn btn-success"></td>
                                </tr>
                                </form>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>

