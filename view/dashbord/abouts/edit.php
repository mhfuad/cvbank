<?php
require_once ("../../../vendor/autoload.php");
use App\dashbord\abouts\Abouts;

$obj = new Abouts();
$user_info = $_SESSION['user_info'];

$arr = $obj->setData($_GET)->edit();

/*echo "<pre>";
print_r($arr);
die();*/

include_once ("../content/head.php");
?>
<body>
<?php  include_once ("../content/main_navbar.php"); ?>
<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <?php  include_once ("../content/sidebar.php"); ?>
        <div class="content-wrapper">
            <div class="content">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <h1>My About</h1>
                        <div class="table-responsive">
                            <table class="table table-bordered table-lg table-hover table-striped">
                                <tbody>
                                <form action="update.php" method="post">
                                    <input name="id" type="hidden" value="<?php echo $arr['id']?>">
                                    <tr>
                                        <td class="col-md-2 col-sm-2">Title</td>
                                        <td><input name="title" type="text" class="form-control" value="<?php echo $arr['title']?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-2 col-sm-2">Phone number</td>
                                        <td><input name="phone" type="text" class="form-control" value="<?php echo $arr['phone']?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-3 col-sm-3">Bio</td>
                                        <td><textarea name="bio" rows="5" cols="3" class="form-control" ><?php echo $arr['bio']?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-2 col-sm-2">Action</td>
                                        <td><input type="submit" value="Update" class="btn btn-success"></td>
                                    </tr>
                                </form>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>

