<?php

require_once ("../../../vendor/autoload.php");
use App\dashbord\abouts\Abouts;

$obj = new Abouts();
$user_info = $_SESSION['user_info'];

$data['user_id'] = $_SESSION['user_info']['unique_id'];

$arr = $obj->setData($data)->show();

/*echo "<pre>";
print_r($arr);
die();*/

include_once ("../content/head.php");
?>
<body>
<?php  include_once ("../content/main_navbar.php"); ?>
<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <?php  include_once ("../content/sidebar.php"); ?>
        <div class="content-wrapper">
            <div class="content">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <h1 class="text-center">About </h1>
                        <div class="table-responsive">
                            <table class="table table-bordered table-lg table-hover table-striped">
                                <tbody>
                                <!--<tr >
                                    <th colspan="6">Control buttons and icons</th>
                                </tr>-->

                                <tr class="active">
                                    <td class="col-md-2 col-sm-2">Title</td>
                                    <td class="col-md-2 col-sm-2">Phone Number</td>
                                    <td class="col-md-3 col-sm-3">Bio</td>
                                    <td class="col-md-2 col-sm-2">Action</td>
                                </tr>
                                <?php foreach($arr as $a){ ?>
                                <tr>
                                    <td><?php echo $a['title']?></td>
                                    <td><?php echo $a['phone']?></td>
                                    <td><?php echo $a['bio']?></td>
                                    <td>
                                        <ul class="icons-list">
                                            <li><a href="edit.php?unique_id=<?php echo $a['user_id'] ?>"><i class="icon-pencil7"></i></a></li>
                                            <!--<li><a href="#"><i class="icon-cog7"></i></a></li>-->
                                            <li><a href="delete.php?unique_id=<?php echo $a['user_id'] ?>"><i class="icon-trash"></i></a></li>
                                        </ul>
                                    </td>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>

<!--

<div class="table-responsive">
    <table class="table table-bordered table-lg">
        <tbody>
        <tr class="active">
            <th colspan="3">Control buttons and icons</th>
        </tr>
        <tr>
            <td class="col-md-2 col-sm-3">Control links</td>
            <td class="col-sm-3">
                <ul class="icons-list">
                    <li><a href="#"><i class="icon-pencil7"></i></a></li>
                    <li><a href="#"><i class="icon-trash"></i></a></li>
                    <li><a href="#"><i class="icon-cog7"></i></a></li>
                </ul>
            </td>
            <td>Basic table row control buttons. These links appear as a list of links with icons</td>
        </tr>
        </tbody>
    </table>
</div>-->