<?php
require_once ("../../../vendor/autoload.php");
use App\dashbord\facts\Facts;

$obj = new Facts();
$arr = $obj->edit($_GET['id']);

$user_info = $_SESSION['user_info'];

/*echo "<pre>";
print_r($arr);
die();*/

include_once ("../content/head.php");
?>
<body>
<?php  include_once ("../content/main_navbar.php"); ?>


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <?php  include_once ("../content/sidebar.php"); ?>
        <div class="content-wrapper">
            <div class="content">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-lg table-hover table-striped">
                                <tbody>
                                <form action="update.php" method="post">
                                    <input type="hidden" name="id" class="form-control" value="<?php echo $arr['id']?>">
                                    <tr>
                                        <td class="col-md-1 col-sm-1">Title</td>
                                        <td><input name="title" class="form-control" type="text" value="<?php echo $arr['title']?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-2 col-sm-2">No_of_Items</td>
                                        <td><input name="no_of_items" class="form-control" type="text" value="<?php echo $arr['no_of_items']?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-3 col-sm-3">Img</td>
                                        <td><input name="img" class="form-control" type="text" value="<?php echo $arr['img']?>"></td>
                                    </tr>

                                    <tr>
                                        <td class="col-md-2 col-sm-2">Action</td>
                                        <td><input type="submit" value="Update" class="btn btn-success"></td>
                                    </tr>
                                </form>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>

