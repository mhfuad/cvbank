<?php
require_once ("../../../vendor/autoload.php");
use App\dashbord\experiences\Experiences;

$obj = new Experiences();
$arr = $obj->edit($_GET['id']);

$user_info = $_SESSION['user_info'];

/*echo "<pre>";
print_r($arr);
die();*/

include_once ("../content/head.php");
?>
<body>
<?php  include_once ("../content/main_navbar.php"); ?>


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <?php  include_once ("../content/sidebar.php"); ?>
        <div class="content-wrapper">
            <div class="content">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-lg table-hover table-striped">
                                <tbody>
                                <form action="update.php" method="post">
                                    <input type="hidden" name="id" class="form-control" value="<?php echo $arr['id']?>">
                                    <tr>
                                        <td class="col-md-2 col-sm-2">Designation</td>
                                        <td><input name="designation" class="form-control" type="text" value="<?php echo $arr['designation']?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-3 col-sm-3">Company Name</td>
                                        <td><input name="company_name" class="form-control" type="text" value="<?php echo $arr['company_name']?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-2 col-sm-2">Start Date</td>
                                        <td><input name="start_date" class="form-control" type="text" value="<?php echo $arr['start_date']?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-2 col-sm-2">End Date</td>
                                        <td><input name="end_date" class="form-control" type="text" value="<?php echo $arr['end_date']?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-2 col-sm-2">Company Location</td>
                                        <td><input name="company_location" class="form-control" type="text" value="<?php echo $arr['company_location']?>"></td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-2 col-sm-2">Action</td>
                                        <td><input type="submit" value="Update" class="btn btn-success"></td>
                                    </tr>
                                </form>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>

