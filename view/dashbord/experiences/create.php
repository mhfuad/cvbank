<?php

session_start();
$user_info = $_SESSION['user_info'];


include_once ("../content/head.php");
?>
<body>
<?php  include_once ("../content/main_navbar.php"); ?>


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <?php  include_once ("../content/sidebar.php"); ?>
        <div class="content-wrapper">
            <div class="content">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <form class="form-horizontal" action="store.php" method="post">
                            <fieldset class="content-group">
                                <legend class="text-bold">About Page</legend>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Designation</label>
                                    <div class="col-lg-10">
                                        <input type="text" name="designation" rows="5" cols="5" class="form-control" placeholder="Description">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Company_name</label>
                                    <div class="col-lg-10">
                                        <input type="text" name="company_name" class="form-control" placeholder="Experience">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Start Date</label>
                                    <div class="col-lg-10">
                                        <input type="text" name="start_date" class="form-control" placeholder="Experience">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">End Date</label>
                                    <div class="col-lg-10">
                                        <input type="text" name="end_date" class="form-control" placeholder="Experience">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Company Location</label>
                                    <div class="col-lg-10">
                                        <input type="text" name="company_location" class="form-control" placeholder="Experience Area">
                                    </div>
                                </div>

                            </fieldset>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
