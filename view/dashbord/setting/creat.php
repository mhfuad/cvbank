<?php

session_start();
$user_info = $_SESSION['user_info'];


include_once ("../content/head.php");
?>
<body>
<?php  include_once ("../content/main_navbar.php"); ?>


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <?php  include_once ("../content/sidebar.php"); ?>
        <div class="content-wrapper">
            <div class="content">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <form class="form-horizontal" action="store.php" method="post">
                            <fieldset class="content-group">
                                <legend class="text-bold">Basic inputs</legend>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Titel</label>
                                    <div class="col-lg-10">
                                        <input type="text" name="title" class="form-control" placeholder="Title">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Full Name</label>
                                    <div class="col-lg-10">
                                        <input type="text" name="fullName" class="form-control" placeholder="Full Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Description</label>
                                    <div class="col-lg-10">
                                        <textarea name="description" rows="5" cols="5" class="form-control" placeholder="Description"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Address</label>
                                    <div class="col-lg-10">
                                        <input type="text" name="address" class="form-control" placeholder="Address">
                                    </div>
                                </div>
                            </fieldset>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
