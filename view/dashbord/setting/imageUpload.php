<?php

session_start();
$user_info = $_SESSION['user_info'];


include_once ("../content/head.php");
?>
<body>
<?php  include_once ("../content/main_navbar.php"); ?>


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <?php  include_once ("../content/sidebar.php"); ?>
        <div class="content-wrapper">
            <div class="content">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <form class="form-horizontal" action="upload.php" method="post" enctype="multipart/form-data">
                            <fieldset class="content-group">
                                <legend class="text-bold">Profile pictur upload</legend>
                                <div class="form-group">
                                    <label class="control-label col-lg-3">Choose your profile pictur</label>
                                    <div class="col-lg-9">
                                        <input type="file" name="fileToUpload" id="fileToUpload" class="form-control">
                                    </div>
                                </div>
                            </fieldset>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
