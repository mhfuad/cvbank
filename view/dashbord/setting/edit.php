<?php
require_once ("../../../vendor/autoload.php");
use App\dashbord\setting\Setting;

$obj = new Setting();
$user_info = $_SESSION['user_info'];

$arr = $obj->edit($_GET['unique_id']);

include_once ("../content/head.php");
?>
<body>
<?php  include_once ("../content/main_navbar.php"); ?>


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <?php  include_once ("../content/sidebar.php"); ?>
        <div class="content-wrapper">
            <div class="content">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <h1>My settings</h1>
                        <div class="table-responsive">
                            <table class="table table-bordered table-lg table-hover table-striped">
                                <tbody>
                                <form action="update.php" method="post">
                                <input name="user_id" type="hidden" value="<?php echo $arr['user_id']?>">

                                <tr>
                                    <td class="col-md-2 col-sm-2">Title</td>
                                    <td><input name="title" type="text" class="form-control" value="<?php echo $arr['title']?>"></td>
                                </tr>
                                <tr>
                                    <td class="col-md-2 col-sm-2">Full Name</td>
                                    <td><input name="fullName" type="text" class="form-control" value="<?php echo $arr['fullname']?>"></td>
                                </tr>
                                <tr>
                                    <td class="col-md-3 col-sm-3">Description</td>
                                    <td><textarea name="description" rows="5" cols="3" class="form-control" ><?php echo $arr['description']?></textarea></td>
                                </tr>
                                <tr>
                                    <td class="col-md-2 col-sm-2">Address</td>
                                    <td><input name="address" type="text" class="form-control" value="<?php echo $arr['address']?>"></td>
                                </tr>
                                <tr>
                                    <td class="col-md-2 col-sm-2">Action</td>
                                    <td><input type="submit" value="Update" class="btn btn-success"></td>
                                </tr>
                                </form>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->

</div>
<!-- /page container -->
</body>
</html>

